# GULP

## Installation

1. Clone the project
2. `npm install`
3. Set up your localhost (orchard.local)
4. `gulp` or `gulp default`



## GULP Commands
| Command | Result |
|:----|----|
| `gulp` or `gulp default`| Will generate the `build` folder
| `gulp watch` | Will initialise BrowserSync and watch files for changes. It will not add or update or remove the images in the  `build` folder
| This is just a very light and simple setup



## Notes
Run `gulp` or `gulp default` when you add or update images and fonts