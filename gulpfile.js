var browserSync = require('browser-sync').create(),
	reload = browserSync.reload;
var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var replace = require('gulp-replace');
var runsequence = require('run-sequence');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var gutil = require('gulp-util');


var config = {
	src: {
		index: 'source/index.html',
		css: 'source/css/**/*.css',
		js: 'source/js/*.js',
		imgs: 'source/images/*',
		fonts: 'source/fonts/**/*',
		sass: 'source/css/**/*.scss'
	},
	build: {
		build_path: 'build',
		index: 'build/index.html',
		css: 'build/css/',
		js: 'build/js/',
		imgs: 'build/images/',
		fonts: 'build/fonts/'
	},
	proxy: 'orchard.local'
};


// Move static files to build
gulp.task('copy-index', function() {
	return gulp.src([config.src.index])
		.pipe(replace('css/style.css', 'css/style.min.css'))
		.pipe(replace('js/script.js', 'js/script.min.js'))
		.pipe(gulp.dest(config.build.build_path));
});

// Copy Fonts
gulp.task('copy-fonts', function() {
	return gulp.src([config.src.fonts])
		.pipe(gulp.dest(config.build.fonts));
});


// Minify images
gulp.task('image:min', function() {
	return gulp.src([config.src.imgs])
		.pipe(imagemin({
			progressive: true,
			use: [imagemin.gifsicle(), imagemin.jpegtran(), imagemin.svgo(), pngquant()]
		}))
		.pipe(gulp.dest(config.build.imgs));
});


// Compile stylesheets
gulp.task('styles', function() {
	return gulp.src([config.src.css, config.src.sass])
		.pipe(plumber(function(error) {
			gutil.log(gutil.colors.red(error.message));
			this.emit('end');
		}))
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(autoprefixer({
			cascade: false
		}))

		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(config.build.css));
});

// Compile scripts
gulp.task('js', function() {
  return gulp.src([config.src.js])
	.pipe(plumber())
	.pipe(jshint())
	.pipe(jshint.reporter('jshint-stylish'))
	.pipe(concat('script.js'))
	.pipe(gulp.dest(config.build.js))
	.pipe(rename({suffix: '.min'}))
	.pipe(uglify())
	.pipe(gulp.dest(config.build.js));
});



// PROCESS TASKS
gulp.task('default', function() {
	runsequence(['copy-index', 'styles', 'js', 'copy-fonts', 'image:min']);
});


gulp.task('watch', function() {
	browserSync.init({
		files: [config.build.build_path + '/js/script.min.js'],
		proxy: config.proxy,
	});
	gulp.watch([config.src.css, config.src.sass], ['styles']).on('change', browserSync.reload);
	gulp.watch([config.src.js], ['js']);
	gulp.watch(config.src.index, ['copy-index']).on('change', browserSync.reload);
	
});