jQuery.noConflict();

(function($)
{
	jQuery(function($)
	{
		var app = {
			init : function()
			{
				app.displayMessageOnClick();	
			},

			displayMessageOnClick : function() {
				$('a').click(function(e){
					e.preventDefault();
					console.log("href: " + $(this).attr('href'));
				});
			},

		};

		// Document ready
		app.init();

	});
})(jQuery);
